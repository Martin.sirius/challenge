import { PrismaClient } from '@prisma/client';

import { CommentRepository } from '.';
import { CreateCommentInputDTO, CommentDTO } from '../dto';

export class CommentRepositoryImpl implements CommentRepository {
  constructor(private readonly db: PrismaClient) {}

  async create(userId: string, postId: string, data: CreateCommentInputDTO): Promise<CommentDTO> {
    const comment = await this.db.comment.create({
      data: {
        authorId: userId,
        postId,
        ...data,
      },
    });
    return new CommentDTO(comment);
  }

  async getByAuthorId(authorId: string, postId: string): Promise<CommentDTO | null> {
    const comment = await this.db.comment.findFirst({
      where: {
        authorId,
        postId,
      },
    });
    return comment ? new CommentDTO(comment) : null;
  }

  async delete(id: string): Promise<void> {
    await this.db.comment.delete({
      where: {
        id,
      },
    });
  }

  //   async getByAuthorId(authorId: string, options: CursorPagination): Promise<PostDTO[]> {
  //     const posts = await this.db.post.findMany({
  //       where: {
  //         authorId,
  //       },
  //       skip: options.before ? Number(options.before) : 0,
  //       take: options.limit ? (options.after ? -options.limit : options.limit) : undefined,
  //     });
  //     return posts.map(post => new PostDTO(post));
  //   }
}
