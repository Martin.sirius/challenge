import { CursorPagination } from '@types';
import { CommentDTO, CreateCommentInputDTO } from '../dto';

export interface CommentRepository {
    create(userId: string, postId: string, data: CreateCommentInputDTO): Promise<CommentDTO>;
    getByAuthorId(authorId: string, postId: string): Promise<CommentDTO | null>;
    delete(id: string): Promise<void>;
}
