import { ReactionDTO } from '@domains/reaction/dto';
import { ExtendedUserDTO } from '@domains/user/dto';
import { IsArray, IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';

export class CreateCommentInputDTO {
  @IsString()
  @IsNotEmpty()
  @MaxLength(240)
  content!: string;

  @IsArray()
  images!: [];

  @IsOptional()
  parentId!: string;
}

export class CommentDTO {
  constructor(comment: CommentDTO) {
    this.id = comment.id;
    this.authorId = comment.authorId;
    this.postId = comment.postId;
    this.content = comment.content;
    this.createdAt = comment.createdAt;
    this.images = comment.images;
    this.replies = comment.replies;
  }

  id: string;
  authorId: string;
  postId: string;
  content: string;
  createdAt: Date;
  replies?: CommentDTO[];
  images: string[];
}
export class CommentWithUserDTO {
  constructor(
    comment: CommentDTO,
    user: ExtendedUserDTO,
    likes: Number,
    rts: Number,
    myLike: boolean = false,
    myRt: boolean = false
  ) {
    this.id = comment.id;
    this.user = user;
    this.postId = comment.postId;
    this.content = comment.content;
    this.createdAt = comment.createdAt;
    this.replies = comment.replies;
    // this.reaction = reaction;
    this.likes = likes;
    this.rts = rts;
    this.images = comment.images;
    this.myLike = myLike;
    this.myRt = myRt;
  }

  id: string;
  // authorId: string;
  user: ExtendedUserDTO;
  postId: string;
  content: string;
  createdAt: Date;
  replies?: CommentDTO[];
  // reaction?: ReactionDTO;
  likes?: Number;
  rts?: Number;
  images: string[];
  myRt?: boolean;
  myLike?: boolean;
}
