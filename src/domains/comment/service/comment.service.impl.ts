import { CreateCommentInputDTO, CommentDTO } from '../dto';
import { CommentRepository } from '../repository';
import { CommentService } from '.';
import { validate } from 'class-validator';
import { NotFoundException } from '@utils';

export class CommentServiceImpl implements CommentService {
    constructor(private readonly repository: CommentRepository) { }

    createComment(userId: string, postId: string, data: CreateCommentInputDTO): Promise<CommentDTO> {
        validate(data);
        return this.repository.create(userId, postId, data);
    }

    async deleteComment(userId: string, postId: string): Promise<void> {
        const comment = await this.repository.getByAuthorId(userId, postId);
        if (!comment) throw new NotFoundException('Comment');
        return this.repository.delete(comment.id);
    }

}
