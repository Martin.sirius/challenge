import { FollowerService } from '@domains/follower/service';
import { CreateCommentInputDTO, CommentDTO } from '../dto';

export interface CommentService {
    createComment(userId: string, postId: string, data: CreateCommentInputDTO): Promise<CommentDTO>;
    deleteComment(userId: string, postId: string): Promise<void>;
}
