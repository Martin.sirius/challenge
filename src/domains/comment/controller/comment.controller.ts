import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import "express-async-errors";

import { db, BodyValidation } from '@utils';

import { CommentRepositoryImpl } from '../repository';
import { CommentService, CommentServiceImpl } from '../service';
import { CreateCommentInputDTO } from '../dto';
import { FollowerService, FollowerServiceImpl } from '@domains/follower/service';
import { FollowerRepositoryImpl } from '@domains/follower/repository';


export const commentRouter = Router();

// Use dependency injection
const service: CommentService = new CommentServiceImpl(new CommentRepositoryImpl(db));
const followService: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db))

/**
 * @openapi
 *   tags:
 *    name: Comment
 * /comment/{post_Id}:
 *   post:
 *     summary: Create comment
 *     tags: [Comment]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post to react
 *        schema:
 *          type: string
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/CreateCommentInputDTO'
 *     responses:
 *       204:
 *         description: Reaction
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CommentDTO'
 *       500:
 *         description: Some server error
 * 
 *   delete:
 *     summary: Delete comment
 *     tags: [Comment]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post to react
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: Reaction
 *       500:
 *         description: Some server error
 *
 */

commentRouter.post('/:postId', BodyValidation(CreateCommentInputDTO), async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { postId } = req.params;
  const data = req.body;

  const post = await service.createComment(userId,postId ,data);

  return res.status(HttpStatus.CREATED).json(post);
});

commentRouter.delete('/:postId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { postId } = req.params;

  await service.deleteComment(userId, postId);

  return res.status(HttpStatus.OK);
});