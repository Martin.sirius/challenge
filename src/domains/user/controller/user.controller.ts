import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import 'express-async-errors';

import { db } from '@utils';

import { UserRepositoryImpl } from '../repository';
import { UserService, UserServiceImpl } from '../service';

/**
 * @openapi
 *   tags:
 *    name: Users
 * /user/:
 *   get:
 *     summary: User
 *     tags: [Users]
 *     parameters:
 *      - in: query
 *        name: limit
 *        required: true
 *        description: The numbers of items to return
 *        schema:
 *          type: string
 *      - in: query
 *        name: skip
 *        required: true
 *        description: The numbers of items to return
 *        schema:
 *          type: string
 *      - in: query
 *        name: search
 *        required: false
 *        description: name to search
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: post of public account or your follower.
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUserDTO'
 *       500:
 *         description: Some server error
 *   delete:
 *     summary: delete user
 *     tags: [Users]
 *     responses:
 *       204:
 *         description: Reaction
 *       500:
 *         description: Some server error
 *
 * /user/me:
 *   get:
 *     summary: User
 *     tags: [Users]
 *     responses:
 *       204:
 *         description: User.
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUserDTO'
 *       500:
 *         description: Some server error
 *
 * /user/{userId}:
 *   get:
 *     summary: User
 *     tags: [Users]
 *     parameters:
 *      - in: path
 *        name: userId
 *        required: true
 *        description: ID of the post
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: specific user.
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUserDTO'
 *       500:
 *         description: Some server error
 * /user/profile_image:
 *   get:
 *     summary: Profile image
 *     tags: [Users]
 *     responses:
 *       204:
 *         description: url of AWS that have the image.
 *       500:
 *         description: Some server error
 *   post:
 *     summary: Profile image
 *     tags: [Users]
 *     responses:
 *       204:
 *         description: url by AWS that you need upload the image
 *       500:
 *         description: Some server error
 *
 *
 */

export const userRouter = Router();

// Use dependency injection
const service: UserService = new UserServiceImpl(new UserRepositoryImpl(db));

userRouter.get('/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { limit, skip, search } = req.query as Record<string, string>;

  const users = await service.getUserRecommendations(userId, { limit: Number(limit), skip: Number(skip) }, search);

  return res.status(HttpStatus.OK).json(users);
});

userRouter.put('/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;

  const data = req.body;

  const userUpdate = await service.updateUser(userId, data);

  return res.status(HttpStatus.OK).json(userUpdate);
});

userRouter.get('/profile_image', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;

  console.log(userId);

  const url = await service.getImage(userId);

  return res.status(HttpStatus.OK).json({ url: url });
});

userRouter.get('/me', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;

  const user = await service.getUser(userId);

  return res.status(HttpStatus.OK).json(user);
});

userRouter.get('/messages/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { limit, skip, search } = req.query as Record<string, string>;

  const users = await service.getUserMessages(userId, { limit: Number(limit), skip: Number(skip) }, search);

  return res.status(HttpStatus.OK).json(users);
});

userRouter.get('/:userId', async (req: Request, res: Response) => {
  const { userId: otherUserId } = req.params;

  const user = await service.getUser(otherUserId);

  return res.status(HttpStatus.OK).json(user);
});

userRouter.delete('/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;

  await service.deleteUser(userId);

  return res.status(HttpStatus.OK);
});

userRouter.post('/profile_image', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;

  const url = await service.uploadImage(userId);

  return res.status(HttpStatus.OK).json({ url: url });
});
