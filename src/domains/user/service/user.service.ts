import { OffsetPagination } from '@types';
import { ExtendedUserDTO, UserDTO } from '../dto';

export interface UserService {
  deleteUser(userId: any): Promise<void>;
  getUser(userId: any): Promise<ExtendedUserDTO>;
  getUserRecommendations(userId: any, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]>;
  uploadImage(userId: string): Promise<string>;
  getImage(userId: string): Promise<string>;
  getUserMessages(userId: any, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]>;
  updateUser(userId: string, data: any): Promise<ExtendedUserDTO>;
}
