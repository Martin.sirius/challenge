import { NotFoundException } from '@utils/errors';
import { OffsetPagination } from 'types';
import { ExtendedUserDTO, UserDTO } from '../dto';
import { UserRepository } from '../repository';
import { UserService } from './user.service';
import * as AWS from 'aws-sdk';

AWS.config.update({ accessKeyId: 'AKIA53BQGYNFFBKCEYM4', secretAccessKey: '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' });
AWS.config.update({ region: 'us-east-2' });
AWS.config.update({ signatureVersion: 'v4' });

export class UserServiceImpl implements UserService {
  constructor(private readonly repository: UserRepository) {}

  async getUser(userId: any): Promise<ExtendedUserDTO> {
    const user = await this.repository.getById(userId);
    if (!user) throw new NotFoundException('user');
    return user;
  }

  getUserRecommendations(userId: any, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]> {
    // TODO: make this return only users followed by users the original user follows
    return this.repository.getRecommendedUsersPaginated(userId, options, search);
  }

  deleteUser(userId: any): Promise<void> {
    return this.repository.delete(userId);
  }

  async getImage(userId: string): Promise<string> {
    const s3 = new AWS.S3();

    const myBucket = 'express-prisma';
    const myKey = `profile_image/${userId}`;
    const signedUrlExpireSeconds = 60 * 5;

    const url = s3.getSignedUrl('getObject', {
      Bucket: myBucket,
      Key: myKey,
      Expires: signedUrlExpireSeconds,
    });

    return url;
  }

  async uploadImage(userId: string): Promise<string> {
    const s3 = new AWS.S3();

    const myBucket = 'express-prisma';
    const myKey = `profile_image/${userId}`;
    const signedUrlExpireSeconds = 60 * 5;

    const url = s3.getSignedUrl('putObject', {
      Bucket: myBucket,
      Key: myKey,
      Expires: signedUrlExpireSeconds,
    });

    this.repository.updateProfilePicture(userId, `https://${myBucket}.s3.us-east-2.amazonaws.com/${myKey}`);

    return url;
  }

  getUserMessages(userId: any, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]> {
    return this.repository.getUserMessages(userId, options, search);
  }

  async updateUser(userId: string, data: any): Promise<ExtendedUserDTO> {
    return this.repository.update(userId, data)
  }
}
