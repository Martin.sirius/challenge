import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UserDTO {
  constructor(user: UserDTO) {
    this.id = user.id;
    this.name = user.name;
    this.createdAt = user.createdAt;
    this.profilePicture = user.profilePicture;
  }

  id: string;
  name: string | null;
  createdAt: Date;
  profilePicture: string | null;
}

export class ExtendedUserDTO extends UserDTO {
  constructor(user: ExtendedUserDTO) {
    super(user);
    this.email = user.email;
    this.username = user.username;
    this.password = user.password;
    this.privacity = user.privacity;
  }

  @IsString()
  email!: string;

  @IsString()
  username!: string;

  @IsString()
  password!: string;

  @IsBoolean()
  privacity!: Boolean;
}
