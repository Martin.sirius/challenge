import { SignupInputDTO } from '@domains/auth/dto';
import { PrismaClient } from '@prisma/client';
import { OffsetPagination } from '@types';
import { ExtendedUserDTO, UserDTO } from '../dto';
import { UserRepository } from './user.repository';

export class UserRepositoryImpl implements UserRepository {
  constructor(private readonly db: PrismaClient) {}

  async create(data: SignupInputDTO): Promise<UserDTO> {
    return await this.db.user
      .create({
        data,
      })
      .then((user) => new UserDTO(user));
  }

  async update(userId: string, data: any): Promise<ExtendedUserDTO> {
    return await this.db.user.update({
      where: { id: userId },
      data: {
        ...data,
      },
    });
  }

  async getById(userId: any): Promise<ExtendedUserDTO | null> {
    const user = await this.db.user.findUnique({
      where: {
        id: userId,
      },
    });
    return user ? new ExtendedUserDTO(user) : null;
  }

  async delete(userId: any): Promise<void> {
    await this.db.user.delete({
      where: {
        id: userId,
      },
    });
  }

  async updateProfilePicture(userId: string, profilePictureUrl: string): Promise<ExtendedUserDTO | null> {
    const updatedUser = await this.db.user.update({
      where: {
        id: userId,
      },
      data: {
        profilePicture: profilePictureUrl,
      },
    });

    return updatedUser ? new ExtendedUserDTO(updatedUser) : null;
  }

  async getRecommendedUsersPaginated(
    userId: string,
    options: OffsetPagination,
    search: string
  ): Promise<ExtendedUserDTO[]> {
    if (search) {
      const users = await this.db.user.findMany({
        where: {
          OR: [
            {
              name: {
                contains: search,
              },
            },
            {
              username: {
                contains: search,
              },
            },
          ],
        },
        take: options.limit,
        skip: options.skip,
        orderBy: [
          {
            id: 'asc',
          },
        ],
      });
      return users.map((user) => new ExtendedUserDTO(user));
    } else {
      const users = await this.db.user.findMany({
        take: options.limit,
        skip: options.skip,
        where: {
          followers: {
            none: {
              followerId: userId,
            },
          },
        },
        orderBy: [
          {
            id: 'asc',
          },
        ],
      });
      return users.map((user) => new ExtendedUserDTO(user));
    }
  }

  async getByEmailOrUsername(email?: string, username?: string): Promise<ExtendedUserDTO | null> {
    const user = await this.db.user.findFirst({
      where: {
        OR: [
          {
            email,
          },
          {
            username,
          },
        ],
      },
    });
    return user ? new ExtendedUserDTO(user) : null;
  }

  async getUserMessages(userId: string, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]> {
    // const users = await this.db.user.findMany({
    //   take: options.limit,
    //   skip: options.skip,
    //   where: {
    //     followers: {
    //       every: {
    //         followerId: userId,
    //       },
    //     },
    //   },
    //   orderBy: [
    //     {
    //       id: 'asc',
    //     },
    //   ],
    // });
    const users = await this.db.user.findMany({
      where: {
        OR: [
          {
            name: {
              contains: search,
            },
          },
          {
            username: {
              contains: search,
            },
          },
        ],
      },
      take: options.limit,
      skip: options.skip,
      orderBy: [
        {
          id: 'asc',
        },
      ],
    });
    return users.map((user) => new ExtendedUserDTO(user));
  }
}
