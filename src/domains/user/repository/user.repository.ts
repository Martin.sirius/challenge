import { SignupInputDTO } from '@domains/auth/dto';
import { OffsetPagination } from '@types';
import { ExtendedUserDTO, UserDTO } from '../dto';

export interface UserRepository {
  create(data: SignupInputDTO): Promise<UserDTO>;
  delete(userId: string): Promise<void>;
  getRecommendedUsersPaginated(userId: string, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]>;
  getUserMessages(userId: string, options: OffsetPagination, search: string): Promise<ExtendedUserDTO[]>;
  getById(userId: string): Promise<ExtendedUserDTO | null>;
  getByEmailOrUsername(email?: string, username?: string): Promise<ExtendedUserDTO | null>;
  updateProfilePicture(userId: string, profilePictureUrl: string): Promise<ExtendedUserDTO | null>;
  update(userId: string, data: any): Promise<ExtendedUserDTO>;
}
