import { CommentDTO, CommentWithUserDTO } from '@domains/comment/dto';
import { ExtendedUserDTO } from '@domains/user/dto';
import { Comment } from '@prisma/client';
// import { Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsNotEmpty,
  isNumber,
  IsOptional,
  IsString,
  MaxLength,
  ValidateNested,
} from 'class-validator';

export class CreatePostInputDTO {
  @IsString()
  @IsNotEmpty()
  @MaxLength(240)
  content!: string;

  @IsOptional()
  // @MaxLength(4)
  images?: string[];
}

export class PostDTO {
  constructor(post: PostDTO) {
    this.id = post.id;
    this.authorId = post.authorId;
    this.content = post.content;
    this.images = post.images;
    this.createdAt = post.createdAt;
  }

  @IsString()
  id: string;

  @IsString()
  authorId: string;

  @IsString()
  content: string;

  @IsString()
  @IsArray()
  images: string[];

  @IsDate()
  createdAt: Date;
}

export class PostWithCommentsDTO {
  constructor(
    post: PostDTO,
    author: ExtendedUserDTO,
    comments: CommentWithUserDTO[],
    likes: number,
    rts: number,
    myLike: boolean = false,
    myRt: boolean = false
  ) {
    this.id = post.id;
    this.user = author;
    this.content = post.content;
    this.images = post.images;
    this.likes = likes;
    this.rts = rts;
    this.createdAt = post.createdAt;
    this.comments = comments;

    this.myLike = myLike;
    this.myRt = myRt;
  }

  @IsString()
  id: string;

  @ValidateNested() // Aseguramos que el tipo sea ExtendedUserDTO
  user: ExtendedUserDTO;

  @IsString()
  content: string;

  @IsString()
  @IsArray()
  images: string[];

  @ValidateNested()
  comments: CommentWithUserDTO[];

  @IsDate()
  createdAt: Date;

  // @isNumber()
  likes: number;

  // @isNumber()
  rts: number;

  myLike?: boolean;

  myRt?: boolean;
}

export class PostWithUser {
  constructor(
    post: PostDTO,
    author: ExtendedUserDTO,
    comments: CommentDTO[],
    likes: number,
    rts: number,
    myLike: boolean = false,
    myRt: boolean = false
  ) {
    this.id = post.id;
    this.user = author;
    this.content = post.content;
    this.images = post.images;
    this.likes = likes;
    this.rts = rts;
    this.createdAt = post.createdAt;
    this.comments = comments;
    this.myLike = myLike;
    this.myRt = myRt;
  }

  @IsString()
  id: string;

  @ValidateNested() // Aseguramos que el tipo sea ExtendedUserDTO
  user: ExtendedUserDTO;

  @IsString()
  content: string;

  @IsString()
  @IsArray()
  images: string[];

  @ValidateNested()
  comments: CommentDTO[];

  @IsDate()
  createdAt: Date;

  // @isNumber()
  likes: number;

  // @isNumber()
  rts: number;

  myLike?: boolean;

  myRt?: boolean;
}
