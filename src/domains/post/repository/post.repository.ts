import { CursorPagination } from '@types';
import { CreatePostInputDTO, PostDTO, PostWithCommentsDTO, PostWithUser } from '../dto';

export interface PostRepository {
  create(userId: string, data: CreatePostInputDTO): Promise<PostDTO>;
  getAllByDatePaginated(userId: string, options: CursorPagination, follow: string): Promise<PostWithUser[]>;
  delete(postId: string): Promise<void>;
  getById(userId: string, postId: string): Promise<PostWithCommentsDTO | null>;
  getByAuthorId(authorId: string, options: CursorPagination): Promise<PostWithUser[]>;
}
