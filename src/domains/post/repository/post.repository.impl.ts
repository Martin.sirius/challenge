import { CommentWithUserDTO } from '@domains/comment/dto';
import { UserDTO } from '@domains/user/dto';
import { PrismaClient } from '@prisma/client';

import { CursorPagination } from '@types';

import { PostRepository } from '.';
import { CreatePostInputDTO, PostDTO, PostWithCommentsDTO, PostWithUser } from '../dto';

export class PostRepositoryImpl implements PostRepository {
  constructor(private readonly db: PrismaClient) {}

  async create(userId: string, data: CreatePostInputDTO): Promise<PostDTO> {
    const post = await this.db.post.create({
      data: {
        authorId: userId,
        ...data,
      },
    });
    return new PostDTO(post);
  }

  async getAllByDatePaginated(userId: string, options: CursorPagination, follow: string): Promise<PostWithUser[]> {
    if (follow === 'true') {
      const posts = await this.db.post.findMany({
        // cursor: {
        //   id: options.after ? options.after : options.before ? options.before : undefined,
        // },
        where: {
          // author: {
          //   followers: {
          //     every: {
          //       followerId: userId,
          //     },
          //   },
          // },

          author: {
            followers: {
              some: {
                followerId: userId,
              },
            },
          },
          // author: {
          //   is: {
          //     followers: {
          //       every: { followerId: userId },
          //     },
          //   },
          // },

          // {
          //   author: {
          //     privacity: false,
          //   },
          // },
        },
        include: {
          author: true,
          Reaction: true,
          Comment: true,
        },
        skip: options.before ? Number(options.before) : 0,
        take: options.limit ? (options.after ? -options.limit : options.limit) : undefined,
        orderBy: [
          {
            createdAt: 'desc',
          },
          {
            id: 'asc',
          },
        ],
      });
      const postsWithUser: PostWithUser[] = posts.map((post) => {
        const likesCount = post.Reaction.filter((reaction) => reaction.like).length;
        const myLike = post.Reaction.filter(
          (reaction) => reaction.authorId === userId && reaction.like && reaction.commentId === null
        );
        const rtsCount = post.Reaction.filter((reaction) => reaction.rt).length;
        const myrt = post.Reaction.filter(
          (reaction) => reaction.authorId === userId && reaction.rt && reaction.commentId === null
        );
        return new PostWithUser(
          post,
          post.author,
          post.Comment,
          likesCount,
          rtsCount,
          myLike.length > 0,
          myrt.length > 0
        );
      });
      return postsWithUser;
    } else {
      const posts = await this.db.post.findMany({
        // cursor: {
        //   id: options.after ? options.after : options.before ? options.before : undefined,
        // },
        where: {
          // authorId:userId,
          AND: [
            {
              author: {
                privacity: false,
              },
            },
            {
              NOT: {
                author: {
                  followers: {
                    some: {
                      followerId: userId,
                    },
                  },
                },
              },
            },
            // {
            //   author: {
            //     is: {
            //       followers: {
            //         some: { followerId: userId },
            //       },
            //     },
            //   },
            // },
          ],
        },
        include: {
          author: true,
          Reaction: true,
          Comment: true,
        },
        skip: options.before ? Number(options.before) : 0,
        take: options.limit ? (options.after ? -options.limit : options.limit) : undefined,
        orderBy: [
          {
            createdAt: 'desc',
          },
          {
            id: 'asc',
          },
        ],
      });
      const postsWithUser: PostWithUser[] = posts.map((post) => {
        const likesCount = post.Reaction.filter((reaction) => reaction.like).length;
        const myLike = post.Reaction.filter(
          (reaction) => reaction.authorId === userId && reaction.like && reaction.commentId === null
        );
        const rtsCount = post.Reaction.filter((reaction) => reaction.rt).length;
        const myrt = post.Reaction.filter(
          (reaction) => reaction.authorId === userId && reaction.rt && reaction.commentId === null
        );
        return new PostWithUser(
          post,
          post.author,
          post.Comment,
          likesCount,
          rtsCount,
          myLike.length > 0,
          myrt.length > 0
        );
      });
      return postsWithUser;
    }
    // return posts.map(post => new PostWithUser(post));
    // const postsWithUser: PostWithUser[] = posts.map(post => new PostWithUser(post, post.author));
  }

  async delete(postId: string): Promise<void> {
    await this.db.post.delete({
      where: {
        id: postId,
      },
    });
  }

  async getById(userId: string, postId: string): Promise<PostWithCommentsDTO | null> {
    const post = await this.db.post.findFirst({
      where: {
        id: postId,
      },
      include: {
        Reaction: true,
        author: true,
      },
    });

    const comment = await this.db.comment.findMany({
      where: {
        postId,
      },
      include: {
        Reaction: true,
        author: true,
        replies: true,
      },
      orderBy: {
        Reaction: {
          _count: 'desc',
        },
      },
      take: 10,
    });

    let likesCount = post?.Reaction.filter((reaction) => reaction.like).length || 0;
    let rtsCount = post?.Reaction.filter((reaction) => reaction.rt).length || 0;

    const myLike =
      post?.Reaction.filter(
        (reaction) => reaction.authorId === userId && reaction.like && reaction.commentId === null
      ) || [];
    const myrt =
      post?.Reaction.filter((reaction) => reaction.authorId === userId && reaction.rt && reaction.commentId === null) ||
      [];

    const comments: CommentWithUserDTO[] = comment.map((com) => {
      console.log(com);
      const likes = com.Reaction.filter((reaction) => reaction.like).length;
      const myLike = com.Reaction.filter((reaction) => reaction.authorId === userId && reaction.like);
      likesCount = likesCount - likes;
      const rts = com.Reaction.filter((reaction) => reaction.rt).length;
      const myrt = com.Reaction.filter((reaction) => reaction.authorId === userId && reaction.rt) || [];
      rtsCount = rtsCount - rts;
      return new CommentWithUserDTO(com, com.author, likes, rts, myLike.length > 0, myrt.length > 0);
    });
    return post
      ? new PostWithCommentsDTO(post, post.author, comments, likesCount, rtsCount, myLike.length > 0, myrt.length > 0)
      : null;
  }

  async getByAuthorId(authorId: string, options: CursorPagination): Promise<PostWithUser[]> {
    const posts = await this.db.post.findMany({
      where: {
        OR: [
          {
            authorId,
          },
          {
            Reaction: {
              some: {
                AND: [
                  {
                    authorId,
                  },
                  {
                    rt: true,
                  },
                ],
              },
            },
          },
        ],
      },
      orderBy: {
        createdAt: 'desc',
      },
      include: {
        Reaction: true,
        Comment: true,
        author: true,
      },
      skip: options.before ? Number(options.before) : 0,
      take: options.limit ? (options.after ? -options.limit : options.limit) : undefined,
    });
    // return posts.map((post) => new PostDTO(post));
    const post: PostWithUser[] = posts.map((post) => {
      const likesCount = post.Reaction.filter((reaction) => reaction.like).length;
      const myLike = post.Reaction.filter(
        (reaction) => reaction.authorId === authorId && reaction.like && reaction.postId === post.id
      );
      const rtsCount = post.Reaction.filter((reaction) => reaction.rt).length;
      const myrt = post.Reaction.filter((reaction) => reaction.authorId === authorId && reaction.rt);
      return new PostWithUser(
        post,
        post.author,
        post.Comment,
        likesCount,
        rtsCount,
        myLike.length > 0,
        myrt.length > 0
      );
    });

    return post;
  }
}
