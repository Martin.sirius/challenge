import { FollowerService } from '@domains/follower/service';
import { CreatePostInputDTO, PostDTO, PostWithCommentsDTO, PostWithUser } from '../dto';

export interface PostService {
  createPost(userId: string, body: CreatePostInputDTO): Promise<PostWithCommentsDTO | null>;
  deletePost(userId: string, postId: string): Promise<void>;
  getPost(userId: string, postId: string, service: FollowerService): Promise<PostWithCommentsDTO>;
  getLatestPosts(
    userId: string,
    options: { limit?: number; before?: string; after?: string },
    follow: string,
  ): Promise<PostWithUser[]>;
  getPostsByAuthor(
    userId: any,
    authorId: string,
    options: { limit?: number; before?: string; after?: string },
    service: FollowerService
  ): Promise<PostWithUser[]>;
  uploadImage(name: string): Promise<any>;
}
