import { CreatePostInputDTO, PostDTO, PostWithCommentsDTO, PostWithUser } from '../dto';
import { PostRepository } from '../repository';
import { PostService } from '.';
import { validate } from 'class-validator';
import { ForbiddenException, NotFoundException } from '@utils';
import { CursorPagination } from '@types';
import { FollowerService } from '@domains/follower/service';
import * as AWS from 'aws-sdk';

AWS.config.update({ accessKeyId: 'AKIA53BQGYNFFBKCEYM4', secretAccessKey: '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' });
AWS.config.update({ region: 'us-east-2' });
AWS.config.update({ signatureVersion: 'v4' });

export class PostServiceImpl implements PostService {
  constructor(private readonly repository: PostRepository) {}

  async createPost(userId: string, data: CreatePostInputDTO): Promise<PostWithCommentsDTO | null> {
    validate(data);
    const newPost = await this.repository.create(userId, data);
    return this.repository.getById(userId, newPost?.id);
  }

  async deletePost(userId: string, postId: string): Promise<void> {
    const post = await this.repository.getById(userId, postId);
    if (!post) throw new NotFoundException('post');
    if (post.user.id !== userId) throw new ForbiddenException();
    return this.repository.delete(postId);
  }

  async getPost(userId: string, postId: string, service: FollowerService): Promise<PostWithCommentsDTO> {
    // TODO: validate that the author has public profile or the user follows the author
    const post = await this.repository.getById(userId, postId);
    if (userId !== String(post?.user.id)) {
      const validate = await service.validateFollow(userId, String(post?.user.id));
      if (!validate) throw new NotFoundException('Private Account');
    }
    if (!post) throw new NotFoundException('post');
    return post;
  }

  getLatestPosts(userId: string, options: CursorPagination, follow: string): Promise<PostWithUser[]> {
    // TODO: filter post search to return posts from authors that the user follows
    return this.repository.getAllByDatePaginated(userId, options, follow);
  }

  async getPostsByAuthor(
    userId: any,
    authorId: string,
    options: CursorPagination,
    service: FollowerService
  ): Promise<PostWithUser[]> {
    // TODO: throw exception when the author has a private profile and the user doesn't follow them
    if (userId != authorId) {
      const validate = await service.validateFollow(userId, authorId);
      if (!validate) throw new NotFoundException('Private Account');
    }
    return this.repository.getByAuthorId(authorId, options);
  }

  async uploadImage(name: string): Promise<any> {
    const s3 = new AWS.S3();

    const myBucket = 'express-prisma';
    const myKey = `post_image/${name}`;
    const signedUrlExpireSeconds = 60 * 5;

    const url = s3.getSignedUrl('putObject', {
      Bucket: myBucket,
      Key: myKey,
      Expires: signedUrlExpireSeconds,
    });

    return {
      presigned: url,
      public: `https://${myBucket}.s3.us-east-2.amazonaws.com/${myKey}`,
    };
  }
}
