import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import 'express-async-errors';

import { db, BodyValidation } from '@utils';

import { PostRepositoryImpl } from '../repository';
import { PostService, PostServiceImpl } from '../service';
import { CreatePostInputDTO } from '../dto';
import { FollowerService, FollowerServiceImpl } from '@domains/follower/service';
import { FollowerRepositoryImpl } from '@domains/follower/repository';

/**
 * @openapi
 *   tags:
 *    name: Posts
 * /post/:
 *   get:
 *     summary: Post
 *     tags: [Posts]
 *     responses:
 *       204:
 *         description: post of public account or your follower.
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PostDTO'
 *       500:
 *         description: Some server error
 *   post:
 *     summary: Create post
 *     tags: [Posts]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PostDTO'
 *     responses:
 *       204:
 *         description: Reaction
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PostDTO'
 *       500:
 *         description: Some server error
 *
 *
 * /post/{postId}:
 *   get:
 *     summary: post
 *     tags: [Posts]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: unfollowed.
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PostWithCommentsDTO'
 *       500:
 *         description: Some server error
 *   delete:
 *     summary: Delete post
 *     tags: [Posts]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post to react
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: Reaction
 *       500:
 *         description: Some server error
 *
 * /post/by_user/{userId}:
 *   get:
 *     summary: post
 *     tags: [Posts]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: post of a user.
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PostWithCommentsDTO'
 *       500:
 *         description: Some server error
 *
 */

export const postRouter = Router();

// Use dependency injection
const service: PostService = new PostServiceImpl(new PostRepositoryImpl(db));
const followService: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db));

postRouter.get('/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { limit, before, after, follow } = req.query as Record<string, string>;

  const posts = await service.getLatestPosts(userId, { limit: Number(limit), before, after },follow);

  return res.status(HttpStatus.OK).json(posts);
});

postRouter.get('/:postId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { postId } = req.params;

  const post = await service.getPost(userId, postId, followService);

  return res.status(HttpStatus.OK).json(post);
});

postRouter.get('/by_user/:userId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { userId: authorId } = req.params;
  const { limit, before, after } = req.query as Record<string, string>;

  const posts = await service.getPostsByAuthor(
    userId,
    authorId,
    { limit: Number(limit), before, after },
    followService
  );

  return res.status(HttpStatus.OK).json(posts);
});

postRouter.post('/', BodyValidation(CreatePostInputDTO), async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const data = req.body;

  const post = await service.createPost(userId, data);

  return res.status(HttpStatus.CREATED).json(post);
});

postRouter.delete('/:postId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { postId } = req.params;

  await service.deletePost(userId, postId);

  return res.status(HttpStatus.OK);
});

postRouter.post(`/image/:name`, async (req: Request, res: Response) => {
  const { name } = req.params;
  const response = await service.uploadImage(name);
  return res.status(HttpStatus.CREATED).json(response);
});
