import { PrismaClient } from '@prisma/client';
import { ReactionDTO } from '../dto';
import { ReactionRepository } from './reaction.repository';

export class ReactionRepositoryImpl implements ReactionRepository {
  constructor(private readonly db: PrismaClient) {}

  async create(data: ReactionDTO): Promise<ReactionDTO> {
    return await this.db.reaction
      .create({
        data: {
          authorId: data.authorId,
          postId: data.postId,
          commentId: data.commentId,
          like: data.like,
          rt: data.rt,
        },
      })
      .then((reaction) => new ReactionDTO(reaction));
  }

  async getById(reactionId: string): Promise<ReactionDTO | null> {
    const reaction = await this.db.reaction.findUnique({
      where: {
        id: reactionId,
      },
    });
    return reaction ? new ReactionDTO(reaction) : null;
  }
  async getByPostId(userId: string, postId: string, like: string, commentId?: string): Promise<ReactionDTO | null> {
    if (commentId) {
      const reaction = await this.db.reaction.findFirst({
        where: {
          authorId: userId,
          like: like === 'true' ? true : false,
          commentId,
        },
      });
      return reaction ? new ReactionDTO(reaction) : null;
    } else {
      const reaction = await this.db.reaction.findFirst({
        where: {
          authorId: userId,
          postId: postId,
          like: like === 'true' ? true : false,
        },
      });
      return reaction ? new ReactionDTO(reaction) : null;
    }
  }

  async delete(id: string): Promise<void> {
    await this.db.reaction.delete({
      where: {
        id,
      },
    });
  }

  async getByAuthorId(userId: string): Promise<any> {
    const reaction = await this.db.reaction.findMany({
      where: {
        post: {
          authorId: userId,
        },
      },
    });
    const likes = reaction.filter((reaction) => reaction.like == true).length;
    const rts = reaction.filter((reaction) => reaction.rt == true).length;
    const comments = reaction.filter((reaction) => reaction.rt == true).length;
    return reaction ? { data: { likes: likes, rts: rts, comments: comments } } : null;
  }
}
