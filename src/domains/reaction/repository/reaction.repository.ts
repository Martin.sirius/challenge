import { ReactionDTO } from '../dto';

export interface ReactionRepository {
  create(data: ReactionDTO): Promise<ReactionDTO>;
  getById(reactionId: string): Promise<ReactionDTO | null>;
  getByPostId(userId: string, postId: string, like: string, commentId?: string): Promise<ReactionDTO | null>;
  delete(reactionId: string): Promise<void>;
  getByAuthorId(userId: string): Promise<any>;
}
