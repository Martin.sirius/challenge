import { IsBoolean, IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';

export class ReactionDTO {

  constructor(reaction: ReactionDTO) {
    this.id = reaction.id;
    this.authorId = reaction.authorId;
    this.postId = reaction.postId;
    this.commentId = reaction.commentId;
    this.like = reaction.like;
    this.rt = reaction.rt;
  }

  id: string;
  authorId: string;
  postId: string;

  @IsString()
  @IsOptional()
  commentId: string | null;

  @IsBoolean()
  @IsOptional()
  like: boolean;

  @IsBoolean()
  @IsOptional()
  rt: boolean;
}
