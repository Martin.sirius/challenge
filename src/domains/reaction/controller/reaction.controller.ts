import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import 'express-async-errors';

import { db } from '@utils';

import { ReactionRepositoryImpl } from '../repository';
import { ReactionService, ReactionServiceImpl } from '../service';
import { UserService, UserServiceImpl } from '@domains/user/service';
import { UserRepositoryImpl } from '@domains/user/repository';
import { PostService, PostServiceImpl } from '@domains/post/service';
import { PostRepositoryImpl } from '@domains/post/repository';
import { FollowerService, FollowerServiceImpl } from '@domains/follower/service';
import { FollowerRepositoryImpl } from '@domains/follower/repository';
import { CommentDTO, CreateCommentInputDTO } from '@domains/comment/dto';

export const reactionRouter = Router();

// Use dependency injection
const service: ReactionService = new ReactionServiceImpl(new ReactionRepositoryImpl(db));
const userService: UserService = new UserServiceImpl(new UserRepositoryImpl(db));
const postService: PostService = new PostServiceImpl(new PostRepositoryImpl(db));
const followService: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db));

/**
 * @openapi
 *   tags:
 *    name: Reaction
 * /reaction/{post_Id}:
 *   post:
 *     summary: Reaction
 *     tags: [Reaction]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post to react
 *        schema:
 *          type: string
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/ReactionDTO'
 *     responses:
 *       204:
 *         description: Reaction
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ReactionDTO'
 *       500:
 *         description: Some server error
 *
 *   delete:
 *     summary: Reaction
 *     tags: [Reaction]
 *     parameters:
 *      - in: path
 *        name: postId
 *        required: true
 *        description: ID of the post to react
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: Reaction
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ReactionDTO'
 *       500:
 *         description: Some server error
 *
 * /reaction/{user_id}:
 *   get:
 *     summary: Reaction
 *     tags: [Reaction]
 *     parameters:
 *      - in: path
 *        name: user_id
 *        required: true
 *        description: ID of the post to react
 *        schema:
 *          type: string
 *
 *     responses:
 *       204:
 *         description: Reaction
 *         content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ReactionDTO'
 *       500:
 *         description: Some server error
 */

reactionRouter.get('/:userId', async (req: Request, res: Response) => {
  // const { userId } = res.locals.context;
  const { userId } = req.params;

  const reactions = await service.getreactionById(userId);

  return res.status(HttpStatus.OK).json(reactions);
});

reactionRouter.post('/:postId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { postId } = req.params;
  const { data } = req.body;
  data.authorId = userId;
  data.postId = postId;

  const user = await service.createReaction(data);

  return res.status(HttpStatus.OK).json(user);
});

reactionRouter.delete('/:postId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { postId } = req.params;
  const { like, commentId } = req.query as Record<string, string>;

  await service.deleteReaction(userId, postId, like, commentId);

  return res.status(HttpStatus.OK);
});
