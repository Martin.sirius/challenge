import { NotFoundException } from '@utils/errors';
import { ReactionDTO } from '../dto';
import { ReactionRepository } from '../repository';
import { ReactionService } from './reaction.service';

export class ReactionServiceImpl implements ReactionService {
  constructor(private readonly repository: ReactionRepository) {}

  createReaction(data: ReactionDTO): Promise<ReactionDTO> {
    return this.repository.create(data);
  }

  async deleteReaction(userId: string, postId: string, like: string, commentId?: string): Promise<void> {
    const reaction = await this.repository.getByPostId(userId, postId, like, commentId);
    if (!reaction) {
      throw NotFoundException;
    }
    console.log(reaction);

    return await this.repository.delete(String(reaction?.id));
  }

  async getreactionById(userId: string): Promise<any> {
    const reaction = await this.repository.getByAuthorId(userId);
    if (!reaction) {
      throw NotFoundException;
    }
    return reaction;
  }
}
