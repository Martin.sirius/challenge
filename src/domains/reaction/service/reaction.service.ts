import { OffsetPagination } from '@types';
import { ReactionDTO } from '../dto';

export interface ReactionService {
  createReaction(data: any): Promise<ReactionDTO>;
  deleteReaction(userId: string, postId: string, like?: string, commentId?: string): Promise<void>;
  getreactionById(userId: string): Promise<any>;
  // deleteUser(userId: any): Promise<void>;
  // getUser(userId: any): Promise<UserDTO>;
  // getUserRecommendations(userId: any, options: OffsetPagination): Promise<UserDTO[]>;
}
