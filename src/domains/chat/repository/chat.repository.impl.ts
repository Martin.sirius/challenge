// import { SignupInputDTO } from '@domains/auth/dto';
import { PrismaClient } from '@prisma/client';
// import { OffsetPagination } from '@types';
import { ChatDTO } from '../dto';
import { ChatRepository } from './chat.repository';

export class ChatRepositoryImpl implements ChatRepository {
    constructor(private readonly db: PrismaClient) { }

    //   async create(data: ChatDTO): Promise<ChatDTO> {
    //     return await this.db.message.create({
    //       data,
    //     }).then(chat => new ChatDTO(chat));
    //   }

    //   async getById(userId: any): Promise<UserDTO | null> {
    //     const user = await this.db.user.findUnique({
    //       where: {
    //         id: userId,
    //       },
    //     });
    //     return user ? new UserDTO(user) : null;
    //   }

    //   async delete(userId: any): Promise<void> {
    //     await this.db.user.delete({
    //       where: {
    //         id: userId,
    //       },
    //     });
    //   }

    //   async getRecommendedUsersPaginated(options: OffsetPagination): Promise<UserDTO[]> {
    //     const users = await this.db.user.findMany({
    //       take: options.limit,
    //       skip: options.skip,
    //       orderBy: [
    //         {
    //           id: 'asc',
    //         },
    //       ],
    //     });
    //     return users.map(user => new UserDTO(user));
    //   }

    //   async getByEmailOrUsername(email?: string, username?: string): Promise<ExtendedUserDTO | null> {
    //     const user = await this.db.user.findFirst({
    //       where: {
    //         OR: [
    //           {
    //             email,
    //           },
    //           {
    //             username,
    //           },
    //         ],
    //       },
    //     });
    //     return user ? new ExtendedUserDTO(user) : null;
    //   }
}
