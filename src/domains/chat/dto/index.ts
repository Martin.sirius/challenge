import { User } from "@prisma/client";
import { dateTimeRangeList } from "aws-sdk/clients/health";

export class ChatDTO {

  constructor(chat: ChatDTO) {
    this.id = chat.id;
    this.authorToId = chat.authorToId;
    this.authorForId = chat.authorForId;
    this.content = chat.content;
    this.createdAt = chat.createdAt;
  }

  id: string;
  authorToId: User;
  authorForId: User;
  content:string;
  createdAt:dateTimeRangeList;

}