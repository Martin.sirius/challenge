import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import 'express-async-errors';

import { db } from '@utils';

import { ChatRepositoryImpl } from '../repository';
import { ChatService, ChatServiceImpl } from '../service';

export const chatRouter = Router();

// Use dependency injection
const service: ChatService = new ChatServiceImpl(new ChatRepositoryImpl(db));

chatRouter.get('/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { limit, skip } = req.query as Record<string, string>;

  //   const users = await service.getUserRecommendations(userId, { limit: Number(limit), skip: Number(skip) });

  return res.status(HttpStatus.OK);
});

chatRouter.post('/:forId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { forId } = req.params;

  //   const url = await service.uploadImage(userId);

  return res.status(HttpStatus.OK);
});
