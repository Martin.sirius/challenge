// import { NotFoundException } from '@utils/errors';
// import { OffsetPagination } from 'types';
// import { UserDTO } from '../dto';
// import { UserRepository } from '../repository';
import { ChatRepository } from '../repository';
import { ChatService } from './chat.service';
// import * as AWS from 'aws-sdk';

// AWS.config.update({ accessKeyId: process.env.AWS_ACCESS_KEY_ID, secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY })
// AWS.config.update({region: 'us-west-2'})

export class ChatServiceImpl implements ChatService {
  constructor(private readonly repository: ChatRepository) { }


//   async getUser(userId: any): Promise<UserDTO> {
//     const user = await this.repository.getById(userId);
//     if (!user) throw new NotFoundException('user');
//     return user;
//   }

//   getUserRecommendations(userId: any, options: OffsetPagination): Promise<UserDTO[]> {
//     // TODO: make this return only users followed by users the original user follows
//     return this.repository.getRecommendedUsersPaginated(options);
//   }

//   deleteUser(userId: any): Promise<void> {
//     return this.repository.delete(userId);
//   }

//   async getImage(userId: string,): Promise<string> {
//     const s3 = new AWS.S3()

//     const myBucket = 'my_bucket'
//     const myKey = `/Profile_image/${userId}`
//     const signedUrlExpireSeconds = 60 * 5

//     const url = s3.getSignedUrl('getObject', {
//       Bucket: myBucket,
//       Key: myKey,
//       Expires: signedUrlExpireSeconds
//     })

//     return url;
//   }

//   async uploadImage(userId: string,): Promise<string> {
//     const s3 = new AWS.S3()

//     const myBucket = 'my_bucket'
//     const myKey = `/Profile_image/${userId}`
//     const signedUrlExpireSeconds = 60 * 5

//     const url = s3.getSignedUrl('putObject', {
//       Bucket: myBucket,
//       Key: myKey,
//       Expires: signedUrlExpireSeconds
//     })

//     return url;
//   }

}
