import { MessageDTO } from "../dto";

export interface MessageRepository {
    create(userIdTo: string, userIdFor: string, content: string, send: boolean): Promise<MessageDTO>;
    getByFor(userTo: string, userFor: string): Promise<MessageDTO[] | null>;
    getByForSend(userId: string): Promise<MessageDTO[]>;
    updateByForSend(userId: string): Promise<any>;
    getChats(userId: string): Promise<any[]>;
}
