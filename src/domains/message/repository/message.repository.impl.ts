import { PrismaClient } from '@prisma/client';
import { MessageDTO } from '../dto';
import { MessageRepository } from './message.repository';

export class MessageRepositoryImpl implements MessageRepository {
  constructor(private readonly db: PrismaClient) {}

  async create(userIdTo: string, userIdFor: string, content: string, send: boolean): Promise<MessageDTO> {
    const message = await this.db.message.create({
      data: {
        authorToId: userIdTo,
        authorForId: userIdFor,
        content: content,
        sending: send,
      },
    });
    return new MessageDTO(message);
  }

  async getByFor(userTo: string, userFor: string): Promise<MessageDTO[] | null> {
    const messages = await this.db.message.findMany({
      where: {
        OR: [
          {
            AND: [
              {
                authorToId: userTo,
              },
              {
                authorForId: userFor,
              },
            ],
          },
          {
            AND: [
              {
                authorToId: userFor,
              },
              {
                authorForId: userTo,
              },
            ],
          },
        ],
      },
      orderBy: {
        createdAt: 'desc',
      },
      take: 30,
    });

    return messages.map((msj) => new MessageDTO(msj));
  }

  async getByForSend(userId: string): Promise<MessageDTO[]> {
    const messages = await this.db.message.findMany({
      where: {
        authorForId: userId,
        sending: false,
      },
      orderBy: {
        createdAt: 'desc',
      },
    });

    return messages.map((msj) => new MessageDTO(msj));
  }

  async updateByForSend(userId: string): Promise<any> {
    await this.db.message.updateMany({
      where: {
        authorForId: userId,
        sending: false,
      },
      data: {
        sending: true,
      },
    });
  }

  async getChats(userId: string): Promise<any[]> {
    const messagesWithLastMessage = await this.db.message.groupBy({
      by: ['authorToId', 'authorForId'],
      where: {
        OR: [{ authorForId: userId }, { authorToId: userId }],
      },
      //   orderBy: { createdAt: 'desc' },
      _count: { content: true },
      _max: { createdAt: true },
    });

    const uniqueCombinations = new Set();

    const messageContents = await Promise.all(
      messagesWithLastMessage.map(async (messageData) => {
        const key1 =
          userId != messageData.authorToId
            ? `${userId}-${messageData.authorToId}-${messageData.authorToId}-${userId}`
            : `${userId}-${messageData.authorForId}-${messageData.authorForId}-${userId}`;

        if (!uniqueCombinations.has(key1)) {
          uniqueCombinations.add(key1);
          const message = await this.db.message.findFirst({
            where: {
              OR: [
                { authorForId: userId, authorToId: messageData.authorToId },
                { authorToId: userId, authorForId: messageData.authorForId },
              ],
            },
            include:{
              authorFor: true,
              authorTo: true,
            },
            orderBy: { createdAt: 'desc' },
          });
          return message
            ? message
            : {
                authorForId: messageData.authorForId,
                authorToId: messageData.authorToId,
                content: '',
              };
        } else {
          return null;
        }
      })
    );

    const filteredMessages = messageContents.filter((message) => message !== null);

    return filteredMessages;
  }
}
