import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import 'express-async-errors';

import { db, BodyValidation } from '@utils';
import { MessageService, MessageServiceImpl } from '../service';
import { MessageRepositoryImpl } from '../repository';

/**
 * @openapi
 *   tags:
 *    name: Messages
 * /messages/{userIdFor}:
 *   get:
 *     summary: message
 *     tags: [Messages]
 *     parameters:
 *      - in: path
 *        name: useridFor
 *        required: true
 *        description: ID of the user to send messages
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: messages.
 *       500:
 *         description: Some server error
 *
 */

export const messagesRouter = Router();

// Use dependency injection
const service: MessageService = new MessageServiceImpl(new MessageRepositoryImpl(db));

messagesRouter.get('/:userIdFor', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { userIdFor } = req.params;

  const messages = await service.getMessages(userId, userIdFor);

  return res.status(HttpStatus.OK).json(messages);
});

messagesRouter.get('/', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;

  const messages = await service.getChats(userId);

  return res.status(HttpStatus.OK).json(messages);
});

// messagesRouter.post('/follow/:followerId', async (req: Request, res: Response) => {

//   const { userId } = res.locals.context;
//   const { followerId } = req.params;

//   const follower = await service.createFollower(userId, followerId);

//   return res.status(HttpStatus.CREATED).json(follower);

// });
