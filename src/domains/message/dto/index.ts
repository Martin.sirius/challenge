

export class MessageDTO {

    constructor(message: MessageDTO) {
        this.id = message.id;
        this.content = message.content;
        this.sending = message.sending;
        this.authorToId = message.authorToId;
        this.authorForId = message.authorForId;
        this.createdAt = message.createdAt;
    }

    id: string;
    content: string;
    sending: boolean;
    authorToId: string;
    authorForId: string;
    createdAt: Date;
}