import { MessageDTO } from "../dto";

export interface MessageService {
    createMessage(userIdTo: string, userIdFor: string, content: string, sending: boolean): Promise<MessageDTO>;
    getMessages(userIdTo: string, userIdFor: string): Promise<MessageDTO[]>;
    getMessagesNotSend(userId: string): Promise<MessageDTO[]>;
    updateMessageNotSend(userId : string): Promise<any>
    getChats(userId: string): Promise<any[]>
}
