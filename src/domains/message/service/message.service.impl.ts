import { NotFoundException } from "@utils";
import { MessageDTO } from "../dto";
import { MessageRepository } from "../repository";
import { MessageService } from "./message.service";


export class MessageServiceImpl implements MessageService {
    constructor(private readonly repository: MessageRepository) { }

    createMessage(userIdTo: string, userIdFor: string, content: string, sending: boolean): Promise<MessageDTO> {
        return this.repository.create(userIdTo, userIdFor, content, sending);
    }

    async getMessages(userIdTo: string, userIdFor: string): Promise<MessageDTO[]> {
        const messages = await this.repository.getByFor(userIdTo, userIdFor);
        if (!messages) throw new NotFoundException('messages')
        return messages;
    }

    async getMessagesNotSend(userId: string): Promise<MessageDTO[]> {
        const messages = await this.repository.getByForSend(userId);
        if (!messages) throw new NotFoundException('messages');

        return messages;
    }

    async updateMessageNotSend(userId: string): Promise<any> {
        await this.repository.updateByForSend(userId);
    }

    async getChats(userId: string): Promise<any[]> {
        return await this.repository.getChats(userId)
    }

}