import { validate } from 'class-validator';
import { ForbiddenException, NotFoundException } from '@utils';
import { CursorPagination } from '@types';
import { FollowerService } from './follower.service';
import { FollowerRepository } from '../repository';
import { FollowerDTO } from '../dto';

export class FollowerServiceImpl implements FollowerService {
  constructor(private readonly repository: FollowerRepository) { }

  createFollower(userId: string, followerId: string): Promise<FollowerDTO> {
    return this.repository.create(userId, followerId);
  }

  async deleteFollower(userId: string, followerId: string): Promise<void> {
    const follow = await this.repository.getByfollow(userId, followerId);
    if (!follow) throw new NotFoundException('follow')
    return this.repository.delete(String(follow?.id));
  }

  async validateFollow(userId: string, follow: string): Promise<Boolean> {
    const validate = await this.repository.getByfollowValidate(userId, follow);
    
    return validate;
  }

}
