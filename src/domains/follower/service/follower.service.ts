import { FollowerDTO } from "../dto";

export interface FollowerService {
  createFollower(userId: string, followedId: string): Promise<FollowerDTO>;
  deleteFollower(userId: string, followerId: string): Promise<void>;
  validateFollow(userId:string, follow:string): Promise<Boolean>;
}
