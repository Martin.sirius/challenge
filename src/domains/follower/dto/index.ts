import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';

export class FollowerDTO {

  constructor(follower: FollowerDTO) {
    this.id = follower.id;
    this.followerId = follower.followerId;
    this.followedId = follower.followedId;
  }

  id: string;
  followerId: string;
  followedId: string;
}
