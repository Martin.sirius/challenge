import { CursorPagination } from '@types';
import { FollowerDTO } from '../dto';

export interface FollowerRepository {
    create(userId: string, followedId: string): Promise<FollowerDTO>;
    delete(id: string): Promise<void>;
    getByfollow(followerId: string, followedId: string): Promise<FollowerDTO | null>;
    getByfollowValidate(followerId: string, followedId: string): Promise<Boolean>;
}
