import { UserDTO } from '@domains/user/dto';
import { PrismaClient } from '@prisma/client';

import { CursorPagination } from '@types';

import { FollowerRepository } from '.';
import { FollowerDTO } from '../dto';

export class FollowerRepositoryImpl implements FollowerRepository {
  constructor(private readonly db: PrismaClient) {}

  async create(userId: string, followedId: string): Promise<FollowerDTO> {
    const follower = await this.db.follow.create({
      data: {
        followerId: userId,
        followedId: followedId,
      },
    });
    return new FollowerDTO(follower);
  }

  async delete(id: string): Promise<void> {
    await this.db.follow.delete({
      where: {
        id: id,
      },
    });
  }

  async getByfollow(followerId: string, followedId: string): Promise<FollowerDTO | null> {
    const follow = await this.db.follow.findMany({
      where: {
        followerId,
        followedId,
      },
    });

    return follow[0] ? new FollowerDTO(follow[0]) : null;
  }

  async getByfollowValidate(follower: string, followed: string): Promise<Boolean> {
    const follow = await this.db.follow.findMany({
      where: {
        OR: [
          {
            followerId: follower,
            followedId: followed,
          },
          {
            followerId: followed,
            followedId: follower,
          },
        ],
      },
    });

    return follow.length > 1 ? true : false;
  }
}
