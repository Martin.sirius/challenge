import { Request, Response, Router } from 'express';
import HttpStatus from 'http-status';
import 'express-async-errors';

import { db, BodyValidation } from '@utils';
import { FollowerService, FollowerServiceImpl } from '../service';
import { FollowerRepositoryImpl } from '../repository';

/**
 * @openapi
 *   tags:
 *    name: Followers
 * /follower/follow/{followedId}:
 *   post:
 *     summary: follow
 *     tags: [Followers]
 *     parameters:
 *      - in: path
 *        name: followedId
 *        required: true
 *        description: ID of the user to follow
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: Followed.
 *       500:
 *         description: Some server error
 *
 * /follower/unfollow/{followerId}:
 *   post:
 *     summary: unfollow
 *     tags: [Followers]
 *     parameters:
 *      - in: path
 *        name: followerId
 *        required: true
 *        description: ID of the user to unfollow
 *        schema:
 *          type: string
 *     responses:
 *       204:
 *         description: unfollowed.
 *       500:
 *         description: Some server error
 *
 * /check/{followerId}:
 *  get:
 *    sumarry: check follow
 *    tags: [Followers]
 */

export const followerRouter = Router();

// Use dependency injection
const service: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db));

followerRouter.get('/', (req: Request, res: Response) => {
  return res.status(HttpStatus.OK).send();
});

followerRouter.get('/check/:followerId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { followerId } = req.params;

  return res.status(HttpStatus.OK).send(await service.validateFollow(userId, followerId));
});

followerRouter.post('/follow/:followerId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { followerId } = req.params;

  const follower = await service.createFollower(userId, followerId);

  return res.status(HttpStatus.CREATED).json(follower);
});

followerRouter.post('/unfollow/:followerId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { followerId } = req.params;

  const follower = await service.deleteFollower(userId, followerId);

  return res.status(HttpStatus.OK).send();
});
