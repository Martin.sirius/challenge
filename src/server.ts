import express from 'express';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import cors from 'cors';

import { Constants, NodeEnv, Logger } from '@utils';
import { router } from '@router';
import { ErrorHandling } from '@utils/errors';

import { createServer } from 'http';
import { Server } from 'socket.io';

import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import { websocketRouter } from '@webSocket';
// import { websocketRouter } from '@websocket';

const app = express();

// Set up request logger
if (Constants.NODE_ENV === NodeEnv.DEV) {
  app.use(morgan('tiny')); // Log requests only in development environments
}

// Set Swagger
const schemas = validationMetadatasToSchemas();
const bodyParser = require('body-parser'),
  swaggerJsdoc = require('swagger-jsdoc'),
  swaggerUi = require('swagger-ui-express');

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Twitter API',
      version: '0.1.0',
      description: 'This is a Twitter clone API',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
    },
    servers: [
      {
        url: 'http://localhost:8080/api',
      },
    ],
    components: {
      schemas: schemas,
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
    security: [{ bearerAuth: [] }],
  },
  apis: ['./src/domains/*/controller/*.ts'],
  basePath: '/api',
};

const specs = swaggerJsdoc(options);

app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(specs, {
    explorer: true,
  })
);

//Set socket
const http = require('http');
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    // origin: ['http://localhost:3000', 'https://challenge-front-sigma.vercel.app', '*'],
    origin:  '*',
    //   // or with an array of origins
    //   // origin: ["https://my-frontend.com", "https://my-other-frontend.com", "http://localhost:3000"],
    credentials: true,
  },
});

websocketRouter(io);

// Set up request parsers
app.use(express.json()); // Parses application/json payloads request bodies
app.use(express.urlencoded({ extended: false })); // Parse application/x-www-form-urlencoded request bodies
app.use(cookieParser()); // Parse cookies

// Set up CORS
app.use(
  cors({
    origin: Constants.CORS_WHITELIST,
  })
);

app.use('/api', router);

app.use(ErrorHandling);

// app.listen(Constants.PORT, () => {
//   Logger.info(`Server listening on port ${Constants.PORT}`);
// });

server.listen(Constants.PORT, () => {
  Logger.info(`Server listening on port ${Constants.PORT}`);
});
