-- AlterTable
ALTER TABLE "Comment" ADD COLUMN     "images" TEXT[],
ADD COLUMN     "parentId" UUID;

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_parentId_fkey" FOREIGN KEY ("parentId") REFERENCES "Comment"("id") ON DELETE SET NULL ON UPDATE CASCADE;
